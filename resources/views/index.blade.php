@extends ('backend.layouts.master')
@section ('title', trans('labels.backend.pages.management'))
@section('page-header')
	<h1>
		{{ trans('labels.backend.pages.management') }}
		<small>{{ trans('labels.backend.pages.active') }}</small>
	</h1>
@endsection

@section('content')
	@include('backend.pages.includes.header-buttons')
	{!! $table->render() !!}
	<div class="clearfix"></div>
@endsection

@section('after-styles-end')
	{!! HTML::style('packages/datatables/jquery.dataTables.min.css') !!}
@endsection

@section('before-scripts-end')
	{!! HTML::script('packages/datatables/jquery.dataTables.min.js') !!}
	{!! $table->script() !!}
@endsection