<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Admin Area</li>
            <li class="">
                <a href="{{ route('clients.create') }}"><i class="fa fa-user"></i><span>Add Client</span></a>
            </li>
            <li class="">
                <a href="{{ route('clients.index') }}"><i class="fa fa-users"></i><span>All Clients</span></a>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>