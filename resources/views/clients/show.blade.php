@extends ('layouts.master')
@section ('title', 'View Client')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <!-- Horizontal Form -->
            <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-yellow">
                    <h3 class="widget-user-username">{{ $client->name }}</h3>
                    <h5 class="widget-user-desc">{{ $client->email }}</h5>
                </div>
                <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                        <li><a href="#">Phone Number <span class="pull-right">{{ $client->phone }}</span></a></li>
                        <li><a href="#">Gender <span class="pull-right">{{ $client->gender }}</span></a></li>
                        <li><a href="#"> Date of Birth <span class="pull-right">{{ $client->dob }}</span></a></li>
                        <li><a href="#">Address <span class="pull-right">{{ $client->address }}</span></a>
                        </li>
                        <li><a href="#">Nationality <span class="pull-right">{{ $client->nationality }}</span></a>
                        </li>
                        <li><a href="#"> Preferred Contact <span class="pull-right">{{ $client->preferred }}</span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- general form elements disabled -->
        </div>
    </div>
@endsection
