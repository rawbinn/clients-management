@extends ('layouts.master')
@section ('title', 'Edit Client')

@section('content')
	<div class="row">
		<div class="col-md-12">
      <!-- Horizontal Form -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Client</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($client, ['route' => ['clients.update', $client->id], 'class' => 'form-horizontal', 'id' => 'client-form', 'role' => 'form', 'method' => 'PATCH']) !!}
            <div class="box-body">
            <div class="form-group">
              {!! Form::label('name', 'Name', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                 {!! Form::text('name', null, ['class' => 'form-control', 'minlength' => '3']) !!}
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                 <div class="radio">
                    {!! Form::radio('gender', 'male',true) !!}
                    Male
                 </div>
                 <div class="radio">
                    {!! Form::radio('gender', 'female') !!}
                    Female
                 </div>
                 </div>
            </div>
            <div class="form-group">
              {!! Form::label('phone', 'Phone Number', ['class' => 'col-sm-2 control-label', 'required']) !!}
              <div class="col-sm-10">
                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('email', 'Email', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('address', 'Address', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::text('address', null, ['class' => 'form-control', 'minlength' => '3']) !!}
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('nationality', 'Nationality', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::text('nationality', null, ['class' => 'form-control']) !!}
              </div>
            </div>
            <div class="form-group">
             {!! Form::label('dob', 'Date of Birth', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::input('date', 'dob', null, ['class' => 'form-control', 'placeholder' => 'yyyy-mm-dd']) !!}
              </div>
            </div>
            <div class="form-group">
             {!! Form::label('preferred', 'Preferred Contact', ['class' => 'col-sm-2 control-label']) !!}
              <div class="col-sm-10">
                {!! Form::select('preferred', [
                     'email' => 'Email',
                     'phone' => 'Phone',
                     'none' => 'None'],
                     null, ['class' => 'form-control']
                  ) !!}
              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            {!! Form::submit('Submit', ['class' => 'btn btn-info pull-right']) !!}
          </div><!-- /.box-footer -->
        {!! Form::close() !!}
      </div><!-- /.box -->
      <!-- general form elements disabled -->
    </div>
	</div>
@endsection

@section('after-scripts-end')
  {!! HTML::script('js/jquery.validate.js') !!}
  <script>
    $(document).ready(function() {
      $("#client-form").validate({
        rules: {
          phone: {
            required: true,
            number: true
          }
        }
      });
    });
  </script>
@endsection
