@extends ('layouts.master')
@section ('title', 'All Client')

@section('content')
	<div class="row">
		<div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">All Client</h3>
                  <div class="pull-right"><a href="{{ route('clients.export.csv') }}" class="btn btn-xs btn-primary">Export as CSV</a></div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Gender</th>
                                    <th>Phone No</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Nationality</th>
                                    <th>DateOfBirth</th>
                                    <th>Preferred Contact</th>
                                    <th class="visible-lg">Created At</th>
                                    <th class="visible-lg">Updated At</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($clients as $client)
                                    <tr>
                                        <td>{!! $client->id !!}</td>
                                        <td>{!! $client->name !!}</td>
                                        <td>{!! $client->gender !!}</td>
                                        <td>{!! $client->phone !!}</td>
                                        <td>{!! link_to("mailto:".$client->email, $client->email) !!}</td>
                                        <td>{!! $client->address !!}</td>
                                        <td>{!! $client->nationality !!}</td>
                                        <td>{!! $client->dob !!}</td>
                                        <td>{!! $client->preferred !!}</td>
                                        <td class="visible-lg">{!! $client->created_at->diffForHumans() !!}</td>
                                        <td class="visible-lg">{!! $client->updated_at->diffForHumans() !!}</td>
                                        <td>
                                            <a href="{{ route('clients.show', $client->id) }}" class="btn btn-xs btn-success"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="View"></i></a>

                                            <a href="{{ route('clients.edit', $client->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>

                                            {!! Form::open(['route' => ['clients.destroy', $client->id], 'method' => 'delete']) !!}
                                                <button onclick="return confirm('Are you sure want to delete this client.')" type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="pull-left">
                        {!! $clients->total() !!}
                        @if ($clients->total()<2)
                            Client
                        @else
                            Clients
                        @endif
                            Total
                    </div>
                    <div class="pull-right">
                        {!! $clients->render() !!}
                    </div>
                </div>
            </div><!-- /.box -->
            <!-- general form elements disabled -->
        </div>
	</div>
@endsection
