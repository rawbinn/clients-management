<?php

namespace App\Http\Controllers\Client;

use App\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientFormRequest;
use Illuminate\Http\Request;

/**
 * Class ClientController
 * @package App\Http\Controllers\Client
 */
class ClientController extends Controller
{
    /**
     * Display a clientsing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::orderBy('name', 'ASC')->paginate(10);
        return view('clients.index')->withClients($clients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientFormRequest $request)
    {
        $client = new Client;
        $client->name = $request->name;
        $client->gender = $request->gender;
        $client->phone = $request->phone;
        $client->email = $request->email;
        $client->address = $request->address;
        $client->nationality = $request->nationality;
        $client->dob = $request->dob;
        $client->preferred = $request->preferred;
        if ($client->save()) {
            return redirect()->back()->withFlashSuccess('New client successfully added.');
        }

        return redirect()->back()->withFlashDanger('Error occured during adding new client.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::find($id);
        return view('clients.show')->withClient($client);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);
        return view('clients.edit')->withClient($client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClientFormRequest $request, $id)
    {
        $client = Client::find($id);
        $client->name = $request->name;
        $client->gender = $request->gender;
        $client->phone = $request->phone;
        $client->email = $request->email;
        $client->address = $request->address;
        $client->nationality = $request->nationality;
        $client->dob = $request->dob;
        $client->preferred = $request->preferred;
        if ($client->save()) {
            return redirect()->back()->withFlashSuccess('Client successfully updated.');
        }

        return redirect()->back()->withFlashDanger('Error occured during updating client.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        if ($client->delete()) {
            return redirect()->back()->withFlashSuccess('Client successfully deleted.');
        }

        return redirect()->back()->withFlashDanger('Error occured during deleting client.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getCsv()
    {
        $headers = [
            'Content-type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename=clients.csv',
        ];

        $clients = Client::all()->toArray();

        # add headers for each column in the CSV download
        array_unshift($clients, array_keys($clients[0]));

        $callback = function () use ($clients) {
            $FH = fopen('php://output', 'w');
            foreach ($clients as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return response()->stream($callback, 200, $headers);
    }
}
