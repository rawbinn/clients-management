<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;

/**
 * Class ClientFormRequest
 * @package App\Http\Requests
 */
class ClientFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:100|min:3',
            'gender' => 'required',
            'phone' => 'required',
            'email' => 'required|email|max:255',
            'address' => 'min:5',
            'dob' => 'date',
        ];
    }
}
