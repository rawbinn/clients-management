<?php

class ClientTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAddClientForm()
    {
        $this->visit('/clients/create')
            ->type('Rabin', 'name')
            ->select('male', 'gender')
            ->type('9840013178', 'phone')
            ->type('rawbinnn@gmail.com', 'email')
            ->type('Basundhara,KTM', 'address')
            ->type('Nepali', 'nationality')
            ->type('1993-08-21', 'dob')
            ->type('phone', 'preferred')
            ->press('Submit')
            ->seePageIs('/clients/create');
    }
}
