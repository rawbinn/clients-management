## Simple Client Section

Simple Client Section is simple web application to store the clients information. This web application can create, read, update, delete the client details. It is build in Laravel Framework 5.2.

### Installation:

- composer install
- Create .env file (example included)
- php artisan key:generate
- php artisan migrate
- php artisan db:seed

### Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
