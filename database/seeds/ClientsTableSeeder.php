<?php

use Carbon\Carbon as Carbon;
use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            'name' => 'Rabin',
            'gender' => 'male',
            'phone' => '9840013178',
            'email' => 'rawbinnn@gmail.com',
            'address' => 'Basundhara, KTM',
            'nationality' => 'Nepali',
            'dob' => '1993-08-21',
            'preferred' => 'Mobile',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
